package au.com.sonymusic.spotlight.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

//import org.apache.commons.lang.StringEscapeUtils;

public class SecurityRequest {

    public static final String AUTH_UTC = "authUtc";
    public static final String AUTH_APP_KEY = "authAppKey";
    public static final String AUTH_APP_SECRET = "authAppSecret";
    public static final String AUTH_SIGNATURE = "authSignature";
    public static final String AUTH_USER_KEY = "authUserKey";
    public static final String AUTH_USER_SECRET = "authUserSecret";
    public static final String COOKIE = "Cookie";

    public static final String RECAPTCHA_CHALLENGE_PARAM_NAME = "recaptcha_challenge_field";
    public static final String RECAPTCHA_RESPONSE_PARAM_NAME = "recaptcha_response_field";

    private Map<String, String> endpointParams;
    private long utc;
    private Set<String> paramExclusionList;

    public SecurityRequest(Map<String, String> params)
    {
	this.endpointParams = params;
	this.utc = new Date().getTime();

	paramExclusionList = new HashSet<String>();
	paramExclusionList.add(RECAPTCHA_CHALLENGE_PARAM_NAME);
	paramExclusionList.add(RECAPTCHA_RESPONSE_PARAM_NAME);
	paramExclusionList.add(AUTH_USER_KEY);
	paramExclusionList.add(AUTH_USER_SECRET);
    }

    public Map<String, String> generateURLParameterMap(String appKey)
    {

	Map<String, String> returnmap = new HashMap<String, String>();
	Iterator<Entry<String, String>> it = endpointParams.entrySet().iterator();

	while (it.hasNext())
	{
	    Entry<String, String> entry = it.next();
	    returnmap.put(entry.getKey(), entry.getValue());
	}
	returnmap.put(AUTH_APP_KEY, appKey);
	return returnmap;

    }

    /**
     * Generate a Map of parameters excluding those on the paramExclusionList 
     * for use when generating an API signature
     * 
     * @param appKey
     * @return
     */
    public Map<String, String> generateURLParameterMapForSignatureGeneration(String appKey)
    {
	Map<String, String> parameterMap = generateEncodedURLParameterMap(appKey, false);

	for (String param : paramExclusionList)
	{
	    parameterMap.remove(param);
	}

	return parameterMap;
    }

    public Map<String, String> generateEncodedURLParameterMap(String appKey)
    {
	return generateEncodedURLParameterMap(appKey, true);
    }

    public Map<String, String> generateEncodedURLParameterMap(String appKey, Boolean urlEncode)
    {

	Map<String, String> returnmap = new HashMap<String, String>();
	Iterator<Entry<String, String>> it = endpointParams.entrySet().iterator();

	while (it.hasNext())
	{
	    Entry<String, String> entry = it.next();

	    String value = entry.getValue();
	    String encodedValue = null;

	    if (value != null && !value.equals(""))
	    {

		try
		{
		    //value = StringEscapeUtils.unescapeHtml(value);

		    String decoded = URLDecoder.decode(value, "UTF-8");
		    if (!urlEncode)
		    { 
			encodedValue = decoded; 
		    } else {
			if (!value.equals(decoded))
			{
			    encodedValue = URLEncoder.encode(decoded, "UTF-8");
			}
			else
			{
			    encodedValue = URLEncoder.encode(value, "UTF-8");
			}
		    }
		}
		catch (UnsupportedEncodingException e)
		{
		    // do nothing
		}

		returnmap.put(entry.getKey(), (encodedValue != null ? encodedValue : value));
	    }
	}

	returnmap.put(AUTH_APP_KEY, appKey);
	return returnmap;

    }

    public long getUTC()
    {
	return utc;
    }

    public Map<String, String> generateHeaderParameterMap(String authSignature, String authUserKey)
    {
	Map<String, String> headers = new HashMap<String, String>();
	headers.put(AUTH_SIGNATURE, authSignature);
	headers.put(AUTH_UTC, "" + getUTC());
	headers.put("Content-Type", "application/json");
	if(authUserKey != null) {
		headers.put(COOKIE, AUTH_USER_KEY + "=" + authUserKey);
	}
	return headers;
    }

}
