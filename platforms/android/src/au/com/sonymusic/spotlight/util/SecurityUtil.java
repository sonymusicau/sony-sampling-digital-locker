package au.com.sonymusic.spotlight.util;

import au.com.bandit.api.common.SignatureUtils;

/**
 * Handles the token cookie used for authenticating users and
 * encrypting/decrypting the token data
 *
 * @author Andrew Carey
 * @author Andreas Borglin
 *
 */
public class SecurityUtil {
    
    // Generate app signature
    public static String generateAppSignature(SecurityRequest securityRequest, String endPoint, String appKey, String appSecret)
    {

	String authSignature = "";

	try
	{
	    SignatureUtils su = new SignatureUtils();
	    authSignature = su.getAppSignature(appSecret, endPoint, securityRequest.getUTC(), securityRequest.generateURLParameterMapForSignatureGeneration(appKey));
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}

	return authSignature;
    }

    // Generate user signature
    public static String generateUserSignature(SecurityRequest securityRequest, String endPoint, String appKey, String appSecret, String userSecretKey)
    {

	String userSignature = "";

	try
	{
	    SignatureUtils su = new SignatureUtils();
	    userSignature = su.getUserSignature(appSecret, userSecretKey, endPoint, securityRequest.getUTC(), securityRequest.generateURLParameterMapForSignatureGeneration(appKey));
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}

	return userSignature;
    }
}
