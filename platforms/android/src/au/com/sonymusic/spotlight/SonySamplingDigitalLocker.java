/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package au.com.sonymusic.spotlight;

import io.stix.Stix;
import io.stix.rest.InitializedListener;
import io.stix.rest.model.AppInstall;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import au.com.sonymusic.datacapture.DataCapture;
import au.com.sonymusic.datacapture.MessageHandler;

import org.apache.cordova.*;

public class SonySamplingDigitalLocker extends CordovaActivity
{
	private static final String STIX_APP_KEY = "TODO"; // TODO:
	private static final String STIX_APP_SECRET = "TODO"; // TODO:
		
	private String endpointArn;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Set by <content src="index.html" /> in config.xml
		loadUrl(launchUrl);
		
		super.appView.addJavascriptInterface(this, "mainActivity");
		
		Stix.initialize(this, this, STIX_APP_KEY, STIX_APP_SECRET,
				new InitializedListener<AppInstall>() {

					@Override
					public void onSuccess(AppInstall result) {
						if (result != null) {
														
							endpointArn = result.getEndpointArn();
																					
							if(result.isNewInstall()) {
								sendData();
							}
						}
					}

					@Override
					public void onFailure(Throwable e) {
					}
				});
		
		MessageHandler handler = new MessageHandler();
		handler.checkForAndHandle(this);
	}

	@JavascriptInterface		
	public String getEndpointArn() {
		return endpointArn;
	}

	private void sendData() {
		DataCapture capture = new DataCapture();
		capture.captureAndSend(this);
	}
}
