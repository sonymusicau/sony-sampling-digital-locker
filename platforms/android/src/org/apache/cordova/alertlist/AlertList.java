package org.apache.cordova.alertlist;

/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/


import java.util.ArrayList;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.AlertDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import au.com.sonymusic.spotlight.R;
/**
 * Alert Dialog with a Message and a List. (Standard AlertDialog only allows Message or List, not both.)
 */
public class AlertList extends CordovaPlugin {
    
	static String TAG = "AlertList";

	private AlertDialog dialog;
	
    /**
     * Constructor.
     */
    public AlertList() {
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action        The action to execute.
     * @param args          JSONArry of arguments for the plugin.
     * @param callbackId    The callback id used when calling back into JavaScript.
     * @return              A PluginResult object with a status and message.
     */
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
            if (action.equals("alertlist")) {
                this.loadList(args, callbackContext);
            }
            return true;
        }

    // --------------------------------------------------------------------------
    // LOCAL METHODS
    // --------------------------------------------------------------------------
    
    public void loadList( final JSONArray dialogItems,  final CallbackContext callbackContext) {
    
        final CordovaInterface cordova = this.cordova;
    	
        Runnable runnable = new Runnable() {
    	
            public void run() {
            	
            	AlertDialog.Builder builder = new AlertDialog.Builder(cordova.getActivity());

                try {
					builder.setTitle(dialogItems.getString(0));
				} catch (JSONException e) {
				}

                LayoutInflater factory = LayoutInflater.from(cordova.getActivity());
                View content = factory.inflate(R.layout.alert_dialog_message_list, null);
                
                TextView tv = (TextView) content.findViewById(R.id.message);
                //tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                try {
					tv.setText(Html.fromHtml(dialogItems.getString(1)));
				} catch (JSONException e) {
				}
                                
                ArrayList<String> listItems = new ArrayList<String>();
                for(int i = 2; i < dialogItems.length(); i++) {
                	try {
						listItems.add(dialogItems.getString(i));
					} catch (JSONException e) {
					}
                }
                                
                ListView lv = (ListView) content.findViewById(R.id.list);
                lv.setAdapter(new ArrayAdapter<String>(cordova.getActivity(),
                        android.R.layout.simple_list_item_1, listItems));
                lv.setChoiceMode(ListView.CHOICE_MODE_NONE);
                
                                                
                lv.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> view, View arg1,
							int position, long arg3) {
						dialog.dismiss();
						callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, position));
					}
				});
                
                builder.setView(content);
                
                dialog = builder.create();
                dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
                dialog.show();
            }
        };
        this.cordova.getActivity().runOnUiThread(runnable);
    }

}