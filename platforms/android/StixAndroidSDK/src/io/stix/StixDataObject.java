package io.stix;

import java.util.HashMap;
import java.util.Map;

import io.stix.rest.RequestBody;

/**
 * Created by James on 1/09/2014.
 */
public class StixDataObject implements RequestBody {

    //private JSONObject main = new JSONObject();
    //private JSONObject data = new JSONObject();
    private Map<String, Object> data = new HashMap<String, Object>();
    private String className;

    public StixDataObject(String className) {
        this.className = className;
    }

    public void put(String name, String value) {
        data.put(name, value);
    }

    public void put(String name, boolean value) {
        data.put(name, value);
    }


    public void put(String name, double value) {
        data.put(name, value);
    }


//    public void put(String name, int value) throws JSONException {
//        return data.put(name, value);
//    }
//
//
//    public JSONObject put(String name, long value) throws JSONException {
//        return data.put(name, value);
//    }
//
//
//    public JSONObject put(String name, Object value) throws JSONException {
//        return data.put(name, value);
//    }
//
//
//    public JSONObject putOpt(String name, Object value) throws JSONException {
//        return data.putOpt(name, value);
//    }


//    public Object remove(String name) {
//        return data.remove(name);
//    }
//
//
//    public boolean isNull(String name) {
//        return !data.containsValue(name);
//    }
//
//
//    public boolean has(String name) {
//        return data.has(name);
//    }
//
//
//    public Object get(String name) throws JSONException {
//        return data.get(name);
//    }
//
//
//    public Object opt(String name) {
//        return data.opt(name);
//    }
//
//
//    public boolean getBoolean(String name) throws JSONException {
//        return data.getBoolean(name);
//    }
//
//
//
//
//    public double getDouble(String name) throws JSONException {
//        return data.getDouble(name);
//    }
//
//
//
//    public double optDouble(String name, double fallback) {
//        return data.optDouble(name, fallback);
//    }
//
//
//    public int getInt(String name) throws JSONException {
//        return data.getInt(name);
//    }
//
//
//
//
//
//    public long getLong(String name) throws JSONException {
//        return data.getLong(name);
//    }


    public String getString(String name) {
        return (String) data.get(name);
    }


//    public String optString(String name) {
//        return data.optString(name);
//    }
//
//
//    public String optString(String name, String fallback) {
//        return data.optString(name, fallback);
//    }
//
//
//    public JSONArray getJSONArray(String name) throws JSONException {
//        return data.getJSONArray(name);
//    }
//
//
//    public JSONArray optJSONArray(String name) {
//        return data.optJSONArray(name);
//    }
//
//
//    public JSONObject getJSONObject(String name) throws JSONException {
//        return data.getJSONObject(name);
//    }
//
//
//    public JSONObject optJSONObject(String name) {
//        return data.optJSONObject(name);
//    }
//
//
//    public JSONArray toJSONArray(JSONArray names) throws JSONException {
//        return data.toJSONArray(names);
//    }

//    public String toString() {
//        return main.toString();
//    }

}
