package io.stix;

/**
 * Created by James on 20/01/2015.
 */
public class AppInfo {
    private String appName;
    private String versionCode;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }
}
