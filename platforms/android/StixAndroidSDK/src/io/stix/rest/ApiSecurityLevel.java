package io.stix.rest;

/**
 * Created by James on 15/02/2015.
 */
public enum ApiSecurityLevel {
    NONE, APP, USER;
}
