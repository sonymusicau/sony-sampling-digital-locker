package io.stix.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import io.stix.Stix;
import io.stix.StixDataObjectBatch;
import io.stix.rest.model.ApiRO;
import io.stix.rest.model.AppError;
import io.stix.rest.model.AppEvent;
import io.stix.rest.model.AppInstall;
import io.stix.rest.model.AppOpen;


public class RestClient {

    private static final String EP_CLIENT_INSTALL = "/1/client/install";
    private static final String EP_CLIENT_OPEN = "/1/client/open";
    private static final String EP_CLIENT_ERROR = "/1/client/error";
    private static final String EP_DATA_SAVE = "/1/data/save";
    private static final String EP_EVENT_SAVE = "/1/event/trackEvent";
    private static final String EP_USER_EVENT_SAVE = "/1/event/trackUserEvent";

    private static final RestClient INSTANCE = new RestClient();

    private static final GsonBuilder builder = new GsonBuilder();
    private static final Gson gson = builder.create();

    private RestClient() {
    }

    public static RestClient getInstance() {
        return INSTANCE;
    }

    public void postAppInstall(AppInstall install, final ResultsListener<AppInstall> aListener) {
        install.setStixVersion(Stix.STIX_VERSION);

        StixRestAPI.getInstance().makeRequest(ApiSecurityLevel.APP, EP_CLIENT_INSTALL, install, AppInstall.class, aListener);
    }


    public void postAppOpened(AppOpen open, final ResultsListener<AppOpen> aListener) {
        StixRestAPI.getInstance().makeRequest(ApiSecurityLevel.APP, EP_CLIENT_OPEN, open, AppOpen.class, aListener);
    }

    public void postAppError(AppError appError, final ResultsListener<ApiRO> aListener) {
        StixRestAPI.getInstance().makeRequest(ApiSecurityLevel.APP, EP_CLIENT_ERROR, appError, null, aListener);
    }

    public void postData(StixDataObjectBatch batch, final ResultsListener<ApiRO> aListener) {
        StixRestAPI.getInstance().makeRequest(ApiSecurityLevel.APP, EP_DATA_SAVE, batch, null, aListener);
    }

    public void postEvent(AppEvent event, final ResultsListener<ApiRO> aListener) {
        StixRestAPI.getInstance().makeRequest(ApiSecurityLevel.APP, EP_EVENT_SAVE, event, null, aListener);
    }

    public void postUserEvent(AppEvent event, final ResultsListener<ApiRO> aListener) {
        StixRestAPI.getInstance().makeRequest(ApiSecurityLevel.USER, EP_USER_EVENT_SAVE, event, null, aListener);
    }

    private String getISODate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(new Date());
    }
}
