package io.stix.rest;

/**
 * Created by James on 9/09/2014.
 */
public class StixMessage {

    public static final String ID = "mId";
    public static final String TITLE = "t";
    public static final String BODY = "m";
    public static final String DATA = "d";
    public static final String ICON = "i";
}
