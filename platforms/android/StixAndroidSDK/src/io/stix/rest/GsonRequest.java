package io.stix.rest;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import io.stix.Stix;

public class GsonRequest<T> extends Request<T> {
    private static final String PROTOCOL_CHARSET = "utf-8";

    /**
     * Content type for request.
     */
    private static final String PROTOCOL_CONTENT_TYPE =
            String.format("application/json; charset=%s", PROTOCOL_CHARSET);

    private final Gson gson = new Gson();
    private final Class<T> responseClazz;
    private final Map<String, String> headers;
    private final Response.Listener<T> listener;

    private final RequestBody<T> requestBody;
    private final List<T> requestListBody;

    /**
     * Make a request and return a parsed object from JSON.
     *
     * @param url           URL of the request to make
     * @param responseClazz Relevant class object, for Gson's reflection
     * @param headers       Map of request headers
     */
    public GsonRequest(String url, RequestBody<T> requestBody, Class<T> responseClazz, Map<String, String> headers,
                       Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(requestBody == null ? Method.GET : Method.POST, url, errorListener);
        this.requestListBody = null;
        this.requestBody = requestBody;
        this.responseClazz = responseClazz;
        this.headers = headers;
        this.listener = listener;
    }

    public GsonRequest(String url, List<T> requestListBody, Class<T> responseClazz, Map<String, String> headers,
                       Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(requestListBody == null ? Method.GET : Method.POST, url, errorListener);
        this.requestBody = null;
        this.requestListBody = requestListBody;
        this.responseClazz = responseClazz;
        this.headers = headers;
        this.listener = listener;
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    protected VolleyError parseNetworkError(VolleyError volleyError) {
        if (volleyError.getCause() == null) {
            VolleyError error = null;

            if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                try {
                    error = new VolleyError(new Exception("" + new String(volleyError.networkResponse.data, "utf-8") + ""));

                } catch (UnsupportedEncodingException e) {
                    Log.e(Stix.TAG, "Unsupported encoding", e);
                }
            }

            if (error == null)
                error = new VolleyError(new Exception("" + volleyError + ""));

            return error;
        } else
            return volleyError;
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {

            if (response.data == null || response.data.length == 0)
                return Response.success(null, HttpHeaderParser.parseCacheHeaders(response));

            String json = new String(
                    response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success(
                    gson.fromJson(json, responseClazz),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    public String getBodyContentType() {
        return PROTOCOL_CONTENT_TYPE;
    }

    @Override
    public byte[] getBody() {
        try {
            if (requestBody == null && requestListBody == null)
                return null;

            return requestBody != null ? gson.toJson(requestBody).getBytes(PROTOCOL_CHARSET) : gson.toJson(requestListBody).getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                    responseClazz, PROTOCOL_CHARSET);
            return null;
        }
    }
}
