package io.stix.rest;

public interface ResultsListener<T> {
    //public void onSuccess(Class<T> clazz);

    public void onSuccess(T result);

    public void onFailure(Throwable e);
}
