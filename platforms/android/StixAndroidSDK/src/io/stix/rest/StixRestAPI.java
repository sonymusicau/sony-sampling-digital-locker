package io.stix.rest;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.SignatureException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.bandit.api.common.SignatureUtils;
import io.stix.Stix;

public class StixRestAPI<T> {

    private static final String LOCAL_DEV_BOOTSTRAP = "http://content.stix.io.s3.amazonaws.com/api-bootstrap/common-bootstrap-james-1.2.4.json";
    private static final String DEV_BOOTSTRAP = "http://content.stix.io.s3.amazonaws.com/api-bootstrap/common-bootstrap-dev-1.2.4.json";
    private static final String BOOTSTRAP = "http://content.stix.io.s3.amazonaws.com/api-bootstrap/common-bootstrap-1.2.4.json";

    private static String baseUrl;
    private static boolean initialized; //Informs that developer has initialised succesfully
    private static String senderId; //GCM Sender ID
    private static String apiKey;
    private static String apiSecret;
    private static String userKey;
    private static String userSecret;
    public static final String USER_ID = "userId";
    public static final String INSTALL_ID = "installId";
    public static final String APP_VERSION = "appVersion";

    public static final String APP_KEY = "apiKey";
    public static final String APP_SECRET = "apiSecret";
    public static final String USER_KEY = "userKey";
    public static final String USER_SECRET = "userSecret";
    public static final String ENDPOINT_ARN = "endpointArn";


    private static final StixRestAPI INSTANCE = new StixRestAPI();
    private RequestQueue mRequestQueue;
    private static final int REQUEST_TIMEOUT = 60000;
    private Context context;

    private static final Gson gson = new GsonBuilder().create();

    private StixRestAPI() {
    }

    public static StixRestAPI getInstance() {
        return INSTANCE;
    }

    public void getInit(Context context, String initApiKey, String initApiSecret, final InitializedListener initializedListener) {
        this.context = context;
        this.apiKey = initApiKey;
        this.apiSecret = initApiSecret;

        if (Stix.getInstall() != null) {
            Log.d(Stix.TAG, "Setting User Key " + Stix.getInstall().getUserKey());
            this.userKey = Stix.getInstall().getUserKey();
            this.userSecret = Stix.getInstall().getUserSecret();
        } else
            Log.d(Stix.TAG, "No User Key");

        String bootstrap = getBootStrapURL();

        //TODO: Refactor to consolidate with lookupApiURLByEnvironment() IF we're keeping it in the SDK
        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response == null || response.equals("")) {
                    Log.e(Stix.TAG, "ERROR Could not initialise Stix as the API bootstrap URL cannot be found");
                } else {
                    JsonParser parser = new JsonParser();
                    JsonObject jo = (JsonObject) parser.parse(response);

                    senderId = jo.get("gcmSenderId").getAsString();
                    baseUrl = jo.get("apiServer").getAsString();

                    initialized = true;
                    initializedListener.onSuccess(null);
                }
            }

        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(Stix.TAG, "Could not retrieve Stix bootstrap.  " + volleyError.getMessage());
                initializedListener.onFailure(new StixRestException("Could not initilize stix. Could not retrieve the bootstrap"));
            }
        };

        StringRequest request = new StringRequest(bootstrap, listener, errorListener);
        request.setRetryPolicy(new DefaultRetryPolicy(REQUEST_TIMEOUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(request);
    }

    /**
     * @param url
     * @param requestBody      Leave null if no request
     * @param responseClazz
     * @param responseListener
     */
    public void makeRequest(ApiSecurityLevel level, String url, RequestBody<T> requestBody, Class<T> responseClazz, final ResultsListener<T> responseListener) {
        Response.ErrorListener errorListener = getErrorListener(responseListener);
        Response.Listener<T> listener = getOkListener(responseListener);
        Map<String, String> headers = getSecureHeaders(level, url, null);

        url = getSecureUrl(level, url);
        Log.d(Stix.TAG, "Making request: " + url);
        GsonRequest gsonRequest = new GsonRequest(url, requestBody, responseClazz, headers, listener, errorListener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_TIMEOUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        getRequestQueue().add(gsonRequest);

    }

    /**
     * @param url
     * @param params           Leave null if no additional parameters
     * @param responseClazz
     * @param responseListener
     */
    public void makeRequest(ApiSecurityLevel level, String url, List<Param> params, Class<T> responseClazz, final ResultsListener<T> responseListener) {
        Response.ErrorListener errorListener = getErrorListener(responseListener);
        Response.Listener<T> listener = getOkListener(responseListener);
        Map<String, String> headers = getSecureHeaders(level, url, params);
        String requestURL = getApiURL(level, url, params);


        RequestBody<T> requestBody = null;
        GsonRequest gsonRequest = new GsonRequest(requestURL, requestBody, responseClazz, headers, listener, errorListener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(0, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(gsonRequest);

    }


    public void makeMultipartRequest(ApiSecurityLevel level, String url, List<Param> params, File file, Class<T> responseClazz, final ResultsListener<T> responseListener) {
        Response.ErrorListener errorListener = getErrorListener(responseListener);
        Response.Listener<T> listener = getOkListener(responseListener);
        Map<String, String> headers = getSecureHeaders(level, url, params);

        String requestURL = getApiURL(level, url, params);


        MultipartRequest request = new MultipartRequest(requestURL, file, Response.class, headers, listener, errorListener);
        request.setRetryPolicy(new DefaultRetryPolicy(0, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        getRequestQueue().add(request);

    }

    private static Map<String, String> getSecureHeaders(ApiSecurityLevel level, String url, List<Param> params) {
        if (level.equals(ApiSecurityLevel.NONE))
            return null;

        long utc = new Date().getTime();
        Map<String, String> mapParams = new HashMap<String, String>();
        if (params != null) {
            for (Param param : params)
                mapParams.put(param.getKey(), param.getValue());
        }

        mapParams.put(APP_KEY, apiKey);
        String signature = null;

        try {
            if (level.equals(ApiSecurityLevel.USER)) {
                if (userKey == null)
                    Log.e(Stix.TAG, "Error. Your attempting to make a user level API call without a user key");
                else
                    mapParams.put(USER_KEY, userKey);

                signature = new SignatureUtils().getUserSignature(apiSecret, userSecret, url, utc, mapParams);
            } else
                signature = new SignatureUtils().getAppSignature(apiSecret, url, utc, mapParams);
        } catch (SignatureException se) {
            Log.e(Stix.TAG, "Could not make secure request", se);
        }
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Signature", signature);
        headers.put("authUtc", String.valueOf(utc));
        return headers;
    }

    private static String getSecureUrl(ApiSecurityLevel level, String url) {
        switch (level) {
            case NONE:
                return baseUrl + url;
            case APP:
                return baseUrl + url + "?" + APP_KEY + "=" + apiKey;
            case USER:
                return baseUrl + url + "?" + APP_KEY + "=" + apiKey + "&" + USER_KEY + "=" + userKey;
            default:
                throw new RuntimeException("Unknown api security level");
        }
    }

    /**
     * FIXME: This is a temp method. All apps should be moved to a method that throws exception on api error response
     *
     * @param level
     * @param endpoint
     * @param params
     * @param responseClass
     * @param <T>
     * @return
     */
    public static <T> T makeSyncGetRequestEx(ApiSecurityLevel level, String endpoint, List<Param> params, Class<T> responseClass) throws StixRestException {

        StringBuilder requestURL = new StringBuilder(getSecureUrl(level, endpoint));
        if (params != null) {
            for (Param param : params) {
                requestURL.append("&");
                requestURL.append(param.toString());
            }
        }
        try {
            HttpURLConnection conn = prepareRequest(level, endpoint, requestURL.toString(), params);
            Log.d(Stix.TAG, "Making request: " + requestURL.toString());
            String response = handleResponse(conn);
            return gson.fromJson(response, responseClass);
        } catch (Exception e) {
            Log.e(Stix.TAG, "Invalid Request", e);
            throw new StixRestException(e);
        }
    }

    public static <T> T makeSyncGetRequest(ApiSecurityLevel level, String endpoint, List<Param> params, Class<T> responseClass) {

        StringBuilder requestURL = new StringBuilder(getSecureUrl(level, endpoint));
        if (params != null) {
            for (Param param : params) {
                requestURL.append("&");
                requestURL.append(param.toString());
            }
        }
        try {
            HttpURLConnection conn = prepareRequest(level, endpoint, requestURL.toString(), params);
            Log.d(Stix.TAG, "Making request: " + requestURL.toString());
            String response = handleResponse(conn);
            return gson.fromJson(response, responseClass);
        } catch (Exception e) {
            Log.e(Stix.TAG, "Invalid Request", e);
        }
        return null;
    }

    public List<?> makeSyncGetRequest(ApiSecurityLevel level, String endpoint, List<Param> params, Type listType) {

        StringBuilder requestURL = new StringBuilder(getSecureUrl(level, endpoint));
        if (params != null) {
            for (Param param : params) {
                requestURL.append("&");
                requestURL.append(param.toString());
            }
        }
        try {
            HttpURLConnection conn = prepareRequest(level, endpoint, requestURL.toString(), params);
            Log.d(Stix.TAG, "Making request: " + requestURL.toString());
            String response = handleResponse(conn);


            return gson.fromJson(response, listType);
        } catch (Exception e) {
            Log.e(Stix.TAG, "Invalid Request", e);
        }
        return null;
    }

    private static String handleResponse(HttpURLConnection conn) throws UnsupportedEncodingException, IOException {
        StringBuilder sb = new StringBuilder();
        InputStream is = conn.getInputStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }
        is.close();
        conn.disconnect();
        return sb.toString();
    }

    private static HttpURLConnection prepareRequest(ApiSecurityLevel level, String endpoint, String url, List<Param> params) throws MalformedURLException, IOException {
        Map<String, String> headers = getSecureHeaders(level, endpoint, params);
        URL u = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) u.openConnection();
        conn.setUseCaches(false);
        conn.addRequestProperty("Signature", headers.get("Signature"));
        conn.addRequestProperty("authUtc", headers.get("authUtc"));
        conn.setReadTimeout(30000);
        return conn;
    }

    private void validateInitialized() {
        if (!initialized)
            throw new RuntimeException("You must call Stix.initialize() before making API requests!");
    }


    public static String getSenderId() {
        return senderId;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(context);
        }

        return mRequestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? Stix.TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(Stix.TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public static boolean isInitialized() {
        return initialized;
    }

    /**
     * TODO: This seems a little dirty here. Should this be exposed here or moved the RedRox SM. That said I suppose we will have dev/prod in stix
     *
     * @param context
     * @param responseListener
     */
    public void lookupApiURLForEnvironment(Context context, final Response.Listener<String> responseListener, final Response.ErrorListener responseErrorListner) {
        this.context = context;


        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JsonParser parser = new JsonParser();
                JsonObject jo = (JsonObject) parser.parse(response);

                senderId = jo.get("gcmSenderId").getAsString();
                baseUrl = jo.get("apiServer").getAsString();

                responseListener.onResponse(baseUrl);
            }

        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                responseErrorListner.onErrorResponse(volleyError);
                Log.e(Stix.TAG, "Could not retrieve Stix bootstrap.  " + volleyError.getMessage());
            }
        };

        StringRequest request = new StringRequest(getBootStrapURL(), listener, errorListener);
        request.setRetryPolicy(new DefaultRetryPolicy(REQUEST_TIMEOUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(request);

    }

    public void makeNoneApiRequest(String url, List<Param> params, Class<T> responseClazz, final ResultsListener<T> responseListener) {

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                if (responseListener != null) {
                    Throwable t = volleyError.getCause();
                    responseListener.onFailure(volleyError.getCause());
                    Log.e(Stix.TAG, volleyError.getCause().getMessage());
                }
            }
        };

        Response.Listener<T> listener = new Response.Listener<T>() {
            @Override
            public void onResponse(T response) {
                if (responseListener != null)
                    responseListener.onSuccess(response);
            }
        };

        StringBuilder requestURL = new StringBuilder(url);
        if (params != null) {
            for (Param param : params) {
                requestURL.append("&");
                requestURL.append(param.toString());
            }
        }

        Log.d(Stix.TAG, "Making request: " + requestURL.toString());
        RequestBody<T> requestBody = null;
        GsonRequest gsonRequest = new GsonRequest(requestURL.toString(), requestBody, responseClazz, null, listener, errorListener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(0, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(gsonRequest);

    }

    private static String getBootStrapURL() {
        String bootstrap = null;
        if (Stix.isDevLocal())
            bootstrap = LOCAL_DEV_BOOTSTRAP;
        else
            bootstrap = Stix.isDev() ? DEV_BOOTSTRAP : BOOTSTRAP;

        return bootstrap;
    }

    public static void setUserKey(String userKey) {
        Log.d(Stix.TAG, "New User Key " + Stix.getInstall().getUserKey());
        StixRestAPI.userKey = userKey;
    }

    public static void setUserSecret(String userSecret) {
        StixRestAPI.userSecret = userSecret;
    }

    private Response.Listener<T> getOkListener(final ResultsListener<T> responseListener) {
        return new Response.Listener<T>() {
            @Override
            public void onResponse(T response) {
                if (responseListener != null)
                    responseListener.onSuccess(response);
            }
        };
    }

    private Response.ErrorListener getErrorListener(final ResultsListener<T> responseListener) {
        return  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (responseListener != null) {
                    Throwable t = volleyError.getCause();
                    responseListener.onFailure(volleyError.getCause());
                }
            }
        };
    }

    private String getApiURL(ApiSecurityLevel level, String url, List<Param> params) {
        StringBuilder requestURL = new StringBuilder(getSecureUrl(level, url));
        if (params != null) {
            for (Param param : params) {
                requestURL.append("&");
                requestURL.append(param.toString());
            }
        }

        Log.d(Stix.TAG, "Making request: " + requestURL);

        return requestURL.toString();
    }

}
