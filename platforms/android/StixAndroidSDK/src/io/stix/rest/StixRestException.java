package io.stix.rest;

/**
 * Created by James on 23/02/2015.
 */
public class StixRestException extends Exception {

    public StixRestException() {
    }

    public StixRestException(String detailMessage) {
        super(detailMessage);
    }

    public StixRestException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public StixRestException(Throwable throwable) {
        super(throwable);
    }
}
