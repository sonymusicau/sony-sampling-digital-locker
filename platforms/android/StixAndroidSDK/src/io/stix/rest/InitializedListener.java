package io.stix.rest;

public interface InitializedListener<T> {
	public void onSuccess(T result);
    public void onFailure(Throwable e);
}
