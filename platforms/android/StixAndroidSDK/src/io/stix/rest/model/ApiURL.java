package io.stix.rest.model;

import io.stix.rest.RequestBody;

/**
 * Created by James on 7/01/2015.
 */
public class ApiURL implements RequestBody {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
