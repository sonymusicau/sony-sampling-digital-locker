package io.stix.rest.model;

import java.util.Map;

import io.stix.rest.RequestBody;

public class AppEvent implements RequestBody {

    private String iId;
    private String uId;
    private Map<String, String> dimensions;
    private String key;
    private String value;

    public AppEvent() {
    }

    public String getiId() {
        return iId;
    }

    public void setiId(String iId) {
        this.iId = iId;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public Map<String, String> getDimensions() {
        return dimensions;
    }

    public void setDimensions(Map<String, String> dimensions) {
        this.dimensions = dimensions;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
