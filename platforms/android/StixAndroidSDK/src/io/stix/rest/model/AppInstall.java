package io.stix.rest.model;

/**
 * Created by James on 26/08/2014.
 */
public class AppInstall extends ApiRO {

    private static final String PUSH_TYPE = "GCM";

    private String uId;
    private String iId;
    private String sId;
    private String appPackage;
    private String appName;
    private String appVersion;
    private String stixVersion;
    private String pushToken;
    private String pushType = PUSH_TYPE;
    private String endpointArn;
    private String timeZone;
    private String deviceId;
    private String deviceType;
    private String deviceMan;
    private String deviceModel;
    private String networkCountry;
    private String networkOperator;
    private String latitude;
    private String longitude;
    private boolean pushEnabled;
    private String ipAddress;
    private String userMetaData;

    private String userKey;
    private String userSecret;

    /**
     * Only used locally *
     */
    private transient boolean isNewInstall;

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getiId() {
        return iId;
    }

    public void setiId(String iId) {
        this.iId = iId;
    }

    public boolean isNewInstall() {
        return isNewInstall;
    }

    public void setNewInstall(boolean isNewInstall) {
        this.isNewInstall = isNewInstall;
    }

    public String getAppPackage() {
        return appPackage;
    }

    public void setAppPackage(String appPackage) {
        this.appPackage = appPackage;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getStixVersion() {
        return stixVersion;
    }

    public void setStixVersion(String stixVersion) {
        this.stixVersion = stixVersion;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public String getPushType() {
        return PUSH_TYPE;
    }


    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getsId() {
        return sId;
    }

    public void setsId(String sId) {
        this.sId = sId;
    }

    public void setPushType(String pushType) {
        this.pushType = pushType;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceMan() {
        return deviceMan;
    }

    public void setDeviceMan(String deviceMan) {
        this.deviceMan = deviceMan;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getNetworkCountry() {
        return networkCountry;
    }

    public void setNetworkCountry(String networkCountry) {
        this.networkCountry = networkCountry;
    }

    public String getNetworkOperator() {
        return networkOperator;
    }

    public void setNetworkOperator(String networkOperator) {
        this.networkOperator = networkOperator;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


    public boolean isPushEnabled() {
        return pushEnabled;
    }

    public void setPushEnabled(boolean pushEnabled) {
        this.pushEnabled = pushEnabled;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public String getUserSecret() {
        return userSecret;
    }

    public void setUserSecret(String userSecret) {
        this.userSecret = userSecret;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUserMetaData() {
        return userMetaData;
    }

    public void setUserMetaData(String userMetaData) {
        this.userMetaData = userMetaData;
    }

	public String getEndpointArn() {
		return endpointArn;
	}

	public void setEndpointArn(String endpointArn) {
		this.endpointArn = endpointArn;
	}
}
