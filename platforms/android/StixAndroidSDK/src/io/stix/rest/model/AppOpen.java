package io.stix.rest.model;

/**
 * Created by James on 14/10/2014.
 * <p/>
 * Note, properties are shortened so they're not apparent
 * to people sniffing the traffic
 */
public class AppOpen extends ApiRO {
    private String uId; //User ID
    private String iId; //Install ID
    private String sId; //Session ID
    private String mId; //Message ID
    private String latitude;
    private String longitude;


    public AppOpen() {
    }

    public AppOpen(AppInstall install) {
        this.uId = install.getuId();
        this.iId = install.getiId();
        this.sId = install.getsId();
        this.latitude = install.getLatitude();
        this.longitude = install.getLongitude();
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getiId() {
        return iId;
    }

    public void setiId(String iId) {
        this.iId = iId;
    }

    public String getsId() {
        return sId;
    }

    public void setsId(String sId) {
        this.sId = sId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }


}
