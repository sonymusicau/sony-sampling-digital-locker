package io.stix.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by James on 17/02/2015.
 */
public class GsonUtil {
    public static final Gson gson = new GsonBuilder().create();
}
