package io.stix.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Created by James on 22/09/2014.
 */
public class FileUtil {

    public static void writeToFile(String filename, String data) throws IOException {
        File file = new File(filename);
        file.createNewFile();
        FileOutputStream o = new FileOutputStream(file);
        o.write(data.getBytes());
        o.close();
    }

    public static String readFromFile(String filename) throws IOException{
        return new Scanner(new File(filename)).useDelimiter("\\A").next();
    }
}
