package io.stix;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.TimeZone;
import java.util.UUID;

import io.stix.rest.InitializedListener;
import io.stix.rest.RestClient;
import io.stix.rest.ResultsListener;
import io.stix.rest.StixMessage;
import io.stix.rest.StixRestAPI;
import io.stix.rest.model.AppInstall;
import io.stix.rest.model.AppOpen;
import io.stix.util.Crypto;
import io.stix.util.FileUtil;
import io.stix.util.IpUtil;

/**
 * Created by James on 23/08/2014.
 * <p/>
 * 1. Checks to see if there is a User ID on the Device from this or another application. If not
 * passed to Stix on the AppInstall, Stix will create new user and return it. This should be
 * saved in an encrytped shared file
 * <p/>
 * 2. Checks to see if their is an existing install id. If there is not, calls install otherwise calls AppOpen
 * to Stix
 * <p/>
 * TODO: Decide. User ID across multiple apps is unusual and specific to Sony's MAN.
 * <p/>
 * TODO: Perhaps use ANDROID_ID for the device id. Then if the device is wiped and given to another user, it's reset
 * (http://developer.android.com/reference/android/provider/Settings.Secure.html#ANDROID_ID)
 * <p/>
 * TODO: Split this out to services. Stix should be the core developer class so it's all in one place for external developers.
 * But then delegates the work out to services
 */
public class Stix {
    public static final String STIX_VERSION = "0.1";
    public static final String TAG = "io.stix";
    public static final String MESSAGE_KEY = "stixMessageData";
    private static String userId;
    private static String appInstallId;
    private static String sessionID;
    private static String deviceId;
    private static AppInstall install;

    private static boolean retry = false;
    private static final int RETRY_MS = 30000;
    private static final String PREF_FILE = "io.stix.prefs";
    private static final String ENCRYPTED_FILENAME = "acpipmi.sys";
    private static final String ENCRYPTION_SEED = "&$(%*&#)_DKS;LDK";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    // private static Context ctx;
    private static String pushMessageId = null;

    //Additional configuration properties. Developer must be instructed to call before initialise
    private static boolean isDevLocal = false; //TODO: Remove this before releasing externally
    private static boolean isDev = false;
    private static boolean pushEnabled = true;
    private static boolean trackGps = true;
    private static boolean secureUserFile = true;
    private static JsonObject userMetaData;

    //Developer can set this if they don't want a shared property file name (ensures they get a unique user id)
    private static String propertyFileName;

    private static Context ctx;
    private static Activity activity;
    private static String apiKey;
    private static String apiSecret;
    private static InitializedListener<AppInstall> initListener;

    /**
     * @param context
     * @param mainActivity
     * @param apiKey
     * @param apiSecret
     */
    public static void initialize(final Context context, Activity mainActivity, String apiKey, String apiSecret) {

        initialize(context, mainActivity, apiKey, apiSecret, null);
    }

    /**
     * @param context
     * @param mainActivity
     * @param apiKey
     * @param apiSecret
     * @param initializedListener
     */
    public static void initialize(Context context, final Activity mainActivity,
                                  final String key, final String secret, final InitializedListener<AppInstall> initializedListener) {
        ctx = context;
        activity = mainActivity;
        apiKey = key;
        apiSecret = secret;
        initListener = initializedListener;
        install = getAppInstallInfo(context);

        init();
    }

    private static void init() {
        //Check to see if the app was opened by a push message
        if (pushEnabled)
            pushMessageId = checkForPushMessageData(activity);


        //Need to initialise API to get the bootstrap
        StixRestAPI.getInstance().getInit(ctx, apiKey, apiSecret, new InitializedListener() {
            @Override
            public void onSuccess(Object result) {
                StixError.sendCrashReport(ctx);
                executeInstallOrOpen(ctx, activity, pushMessageId, initListener);
            }

            @Override
            public void onFailure(Throwable e) {
                Log.e(TAG, "Could not initialise API client. Scheduling a retry. Reason: " + e.getMessage(), e);
                scheduleRetryOfInit();
            }
        });
    }

    private static void scheduleRetryOfInit() {
        if (retry) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    init();
                }
            }, RETRY_MS);
        }
    }

    private static String checkForPushMessageData(Activity mainActivity) {
        Bundle extras = mainActivity.getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(MESSAGE_KEY)) {
                String data = extras.getString(MESSAGE_KEY);
                try {
                    JSONObject msg = new JSONObject(data);
                    return msg.getString(StixMessage.ID);
                } catch (Exception e) {
                    Log.e(TAG, "Check message error  " + e.getMessage(), e);
                }
            }
        }

        return null;
    }


    private static void executeInstallOrOpen(Context context, Activity mainActivity, String pushMessageId, InitializedListener<AppInstall> initializedListener) {
        ctx = context;

        initListener = initializedListener;

        install.setsId(UUID.randomUUID().toString());

        if (trackGps)
            install = retrieveLocation(context, install);

        if (install.getiId() == null) {
            prepareAndInstall(mainActivity, context, install);
        } else {
            //TODO: Method to check install version vs app version and reregister GCM if needed
            createOpenEvent(context, install, pushMessageId);
        }

    }


    /**
     * TODO Test on various devices to see if this is accurate enough without registering a gps listerner.
     * Doing this is slow. We could look at doing an update Perhaps we want to do an update of the install/open
     *
     * @param context
     * @param install
     * @return
     */
    private static AppInstall retrieveLocation(Context context, AppInstall install) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        if (location != null) {
            install.setLatitude(String.valueOf(location.getLatitude()));
            install.setLongitude(String.valueOf(location.getLongitude()));
        }

        return install;
    }

    private static void createInstall(final AppInstall install, final Context context) {
        RestClient.getInstance().postAppInstall(install, new ResultsListener<AppInstall>() {
            @Override
            public void onSuccess(AppInstall newInstall) {
                newInstall.setNewInstall(true);
                saveAppInstallInfo(context, newInstall);

                if (initListener != null)
                    initListener.onSuccess(newInstall);

                Log.d(TAG, "App Installed");

            }

            @Override
            public void onFailure(Throwable e) {
                Log.e(TAG, "Install Error: " + e.getMessage(), e);
                scheduleRetryOfInit();
            }

        });
    }


    private static void createOpenEvent(Context context, final AppInstall install, final String pushMessageId) {
        AppOpen open = new AppOpen(install);
        open.setmId(pushMessageId);


        RestClient.getInstance().postAppOpened(open, new ResultsListener<AppOpen>() {
            @Override
            public void onSuccess(AppOpen open) {

                Log.d(TAG, "App Open Sent  ");

                if (initListener != null)
                    initListener.onSuccess(install);
            }

            @Override
            public void onFailure(Throwable e) {
                Log.e(TAG, e.getMessage(), e);
                scheduleRetryOfInit();
            }
        });

    }

    private static AppInstall prepareAndInstall(Activity activity, Context context, AppInstall install) {
        try {

            PackageManager pm = context.getPackageManager();
            PackageInfo pInfo = pm.getPackageInfo(context.getApplicationInfo().packageName, 0);

            install.setAppName(pInfo.applicationInfo.loadLabel(pm).toString());
            install.setAppPackage(context.getApplicationInfo().packageName);
            install.setAppVersion(pInfo.versionName);
            install.setStixVersion(STIX_VERSION);
            install.setTimeZone(TimeZone.getDefault().getID());
            install.setDeviceId(Settings.System.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
            install.setDeviceType(Build.TYPE);
            install.setDeviceMan(Build.MANUFACTURER);
            install.setDeviceModel(Build.MODEL);

            if (userMetaData != null)
                install.setUserMetaData(userMetaData.toString());

            install.setPushEnabled(pushEnabled);
            install.setIpAddress(IpUtil.getLocalIpAddress());

            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            install.setNetworkCountry(tm.getNetworkCountryIso());
            install.setNetworkOperator(tm.getNetworkOperatorName());

            if (!pushEnabled) {
                createInstall(install, context);
            } else if (checkPlayServices(activity, context)) {
                registerGcmInBackground(install, context);
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }

        return install;
    }

    private static void registerGcmInBackground(AppInstall install, final Context context) {

        new AsyncTask<AppInstall, Void, AppInstall>() {

            @Override
            protected AppInstall doInBackground(AppInstall... params) {
                AppInstall install = params[0];

                try {
                    GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
                    String regId = gcm.register(StixRestAPI.getSenderId());
                    Log.d(TAG, "Device registered, registration ID=" + regId);

                    install.setPushToken(regId);

                } catch (IOException e) {
                    Log.e(TAG, e.getMessage(), e);
                }

                return install;
            }

            @Override
            protected void onPostExecute(AppInstall install) {
                createInstall(install, context);
            }

        }.execute(install, null, null);
    }

    private static boolean checkPlayServices(Activity activity, Context context) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, activity, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                //TODO: What do we do here?? Basically we can't proceed
            }
            return false;
        }
        return true;
    }

    /**
     * Attempts to get User ID and InstallId. If either are null we'll be calling
     * Stix to create the install and user if required
     * <p/>
     * TODO: This should read from an encrypted file shared between the apps. However, this
     * is out of scope with Stix??
     *
     * @param context
     * @return
     */
    private static AppInstall getAppInstallInfo(Context context) {
        AppInstall install = new AppInstall();

        SharedPreferences prefs = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        install.setuId(prefs.getString(StixRestAPI.USER_ID, null));
        install.setiId(prefs.getString(StixRestAPI.INSTALL_ID, null));
        install.setAppVersion(prefs.getString(StixRestAPI.APP_VERSION, null));
        install.setUserKey(prefs.getString(StixRestAPI.USER_KEY, null));
        install.setUserSecret(prefs.getString(StixRestAPI.USER_SECRET, null));
        install.setEndpointArn(prefs.getString(StixRestAPI.ENDPOINT_ARN, null));

        if (install.getuId() == null) {
            // See if we can find it in the shared encryped file
            File file = new File(getFileFolder().getAbsolutePath() + File.separator + getPropertyFileName());

            if (file.isFile()) {
                try {
                    String userId = FileUtil.readFromFile(file.getAbsolutePath());

                    if (secureUserFile) {
                        Crypto c = new Crypto();
                        install.setuId(new String(c.decrypt(userId)).trim());
                    } else
                        install.setuId(userId);
                } catch (Exception e) {
                    Log.e(TAG, "Error with encryption file", e);
                }
            }
        }


        return install;
    }

    /**
     * TODO: This should write to a private encrypted file
     *
     * @param context
     * @param install
     */
    private static void saveAppInstallInfo(Context context, AppInstall newInstall) {
        install = newInstall;

        SharedPreferences prefs = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(StixRestAPI.USER_ID, install.getuId());
        editor.putString(StixRestAPI.INSTALL_ID, install.getiId());
        editor.putString(StixRestAPI.APP_VERSION, install.getAppVersion());
        editor.putString(StixRestAPI.ENDPOINT_ARN, install.getEndpointArn());

        if (install.getUserKey() != null && install.getUserSecret() != null) {
            editor.putString(StixRestAPI.USER_KEY, install.getUserKey());
            editor.putString(StixRestAPI.USER_SECRET, install.getUserSecret());

            //Set these straight away so we can start making user level request
            StixRestAPI.setUserKey(install.getUserKey());
            StixRestAPI.setUserSecret(install.getUserSecret());
        }

        editor.commit();


        try {
            String userId = null;

            if (secureUserFile) {

                //Also save the userId (encrypted) to a obscure file so it can be read by other apps
                Crypto c = new Crypto();
                userId = Crypto.bytesToHex(c.encrypt(install.getuId()));
            } else
                userId = install.getuId();

            File folder = getFileFolder();
            if (!folder.isDirectory())
                folder.mkdirs();


            FileUtil.writeToFile(folder.getAbsolutePath() + File.separator + getPropertyFileName(), userId);


        } catch (Exception e) {
            Log.e(TAG, "Error with encryption", e);
        }

        Log.d(TAG, "App Installed. Install ID: " + install.getiId());
        Log.d(TAG, "User ID: " + install.getuId());
        Log.d(TAG, "User Key: " + install.getUserKey());
    }


    private static File getFileFolder() {
        return new File(Environment.getExternalStorageDirectory() + File.separator + "Android" + File.separator + "Data");
    }

    public static AppInstall getInstall() {
        return install;
    }
//
//    public static Context getContext() {
//        return ctx;
//    }

    public static boolean isDev() {
        return isDev;
    }

    public static void setIsDev(boolean isDev) {
        Stix.isDev = isDev;
    }

    public static boolean isPushEnabled() {
        return pushEnabled;
    }

    public static void setPushEnabled(boolean pushEnabled) {
        Stix.pushEnabled = pushEnabled;
    }

    public static boolean trackGps() {
        return trackGps;
    }

    public static void setTrackGps(boolean trackGps) {
        Stix.trackGps = trackGps;
    }

    public static JsonObject getUserMetaData() {
        return userMetaData;
    }

    public static void setUserMetaData(JsonObject userMetaData) {
        Stix.userMetaData = userMetaData;
    }

    public static boolean isInitialized() {
        return StixRestAPI.isInitialized() && getInstall() != null && getInstall().getiId() != null;
    }

    public static boolean isDevLocal() {
        return isDevLocal;
    }

    public static void setIsDevLocal(boolean isDevLocal) {
        Stix.isDevLocal = isDevLocal;
    }


    public static void setSecureUserFile(boolean secureUserFile) {
        Stix.secureUserFile = secureUserFile;
    }

    public static String getPropertyFileName() {
        if (propertyFileName != null) {
            if (isDevLocal() || isDev())
                return "dev-" + propertyFileName;
            else
                return propertyFileName;
        } else
            return ENCRYPTED_FILENAME;
    }

    public static void setPropertyFileName(String propertyFileName) {
        Stix.propertyFileName = propertyFileName;
    }


    public static void setRetry(boolean retry) {
        Stix.retry = retry;
    }

    /**
     * Removes all stix data to reset account
     */
    public static boolean resetStix() {
        install = new AppInstall();
        File propFile = new File(getFileFolder() + File.separator + getPropertyFileName());
        boolean deleted = false;
        if (propFile.isFile())
            deleted = propFile.delete();

        if (!deleted)
            return false;

        SharedPreferences prefs = ctx.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(StixRestAPI.USER_ID);
        editor.remove(StixRestAPI.INSTALL_ID);
        editor.remove(StixRestAPI.APP_VERSION);
        editor.remove(StixRestAPI.USER_KEY);
        editor.remove(StixRestAPI.USER_SECRET);

        StixRestAPI.setUserKey(null);
        StixRestAPI.setUserSecret(null);

        editor.commit();


        return true;
    }
}






























