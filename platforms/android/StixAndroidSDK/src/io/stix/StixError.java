package io.stix;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;

import io.stix.rest.RestClient;
import io.stix.rest.model.AppError;
import io.stix.rest.model.AppInstall;
import io.stix.util.GsonUtil;

/**
 * Created by James on 17/10/2014.
 */
public class StixError {
    private static final String ERROR_PREF_FILE = "errorPrefFile";
    private static final String PROP_ERROR = "error";


    private static AppInfo appInfo = null;

    public static void logError(Context context, boolean notify, String errorMessage) {
        logError(context, notify, errorMessage, null);
    }

    public static void logError(Context context, boolean notify, Throwable ex) {

        logError(context, notify, null, ex);
    }

    public static void logError(Context context, boolean notify, String errorMessage, Throwable ex) {
        logError(context, notify, errorMessage, ex, true);
    }

    public static AppError logError(Context context, boolean notify, String errorMessage, Throwable ex, boolean immediateSend) {
        AppError appError = new AppError();

        AppInstall install = Stix.getInstall();
        if (install == null) {
            if (errorMessage == null)
                errorMessage = "";

            throw new RuntimeException("Could not log error as Stix User is not yet registered. Error: " + errorMessage, ex);
        }

        appError.setiId(install.getiId());
        appError.setuId(install.getuId());
        appError.setUserKey(install.getUserKey());
        appError.setAppName(getAppInfo(context, errorMessage, ex).getAppName());
        appError.setAppVersion(getAppInfo(context, errorMessage, ex).getVersionCode());

        if (ex != null) {
            final StringWriter sw = new StringWriter();
            final PrintWriter pw = new PrintWriter(sw, true);
            ex.printStackTrace(pw);
            appError.setStackTrace(sw.getBuffer().toString());
        }

        if (errorMessage == null && ex == null)
            logError(context, notify, "Unknown Error. Exception was null", null);
        else
            appError.setErrorMessage(errorMessage);

        appError.setNotify(notify);

        if (immediateSend)
            RestClient.getInstance().postAppError(appError, null);

        return appError;
    }


    /**
     * Sends the same error information as logError. However, this method saves the error
     * and sends it on the app restart. If you you logError to send crash reports, there are no
     * garuntees that the error will be send before the app crashes
     *
     * @param context
     * @param notify
     * @param errorMessage
     * @param ex
     */
    public static void logCrash(Context context, boolean notify, String errorMessage, Throwable ex) {
        AppError error = logError(context, notify, errorMessage, ex, false);
        String errorString = GsonUtil.gson.toJson(error);
        SharedPreferences prefs = context.getSharedPreferences(ERROR_PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString(PROP_ERROR, errorString);

        editor.commit();
    }

    public static void sendCrashReport(Context context){
        SharedPreferences prefs = context.getSharedPreferences(ERROR_PREF_FILE, Context.MODE_PRIVATE);
        try {
            if (prefs.getString(PROP_ERROR, null) != null) {
                SharedPreferences.Editor editor = prefs.edit();
                AppError error = GsonUtil.gson.fromJson(prefs.getString(PROP_ERROR, null), AppError.class);
                RestClient.getInstance().postAppError(error, null);
                editor.remove(PROP_ERROR);
                editor.commit();
            }
        }
        catch(Exception e){
            Log.e(Stix.TAG, "Could not send crash", e);
        }
    }
    private static AppInfo getAppInfo(Context context, String errorMessage, Throwable ex) {
        if (appInfo == null) {
            try {
                appInfo = new AppInfo();
                PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getApplicationInfo().packageName, 0);
                appInfo.setAppName(pInfo.applicationInfo.loadLabel(context.getPackageManager()).toString());
                appInfo.setVersionCode(pInfo.versionName);
            } catch (Exception e) {
                //Can't log error so this is fatal
                if (errorMessage == null)
                    errorMessage = "";

                throw new RuntimeException("Could not log error to Stix: " + errorMessage, ex);
            }

        }

        return appInfo;
    }
}
