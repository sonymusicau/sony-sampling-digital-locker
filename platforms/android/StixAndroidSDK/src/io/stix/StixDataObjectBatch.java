package io.stix;

import java.util.List;

import io.stix.rest.RequestBody;

/**
 * Created by James on 15/10/2014.
 */
public class StixDataObjectBatch implements RequestBody {
    private String sId;
    private String iId;
    private List<StixDataObject> objectArray;

    public StixDataObjectBatch() {
    }

    public StixDataObjectBatch(String sId, String iId, List<StixDataObject> objectArray) {
        this.sId = sId;
        this.iId = iId;
        this.objectArray = objectArray;
    }

    public String getsId() {
        return sId;
    }

    public void setsId(String sId) {
        this.sId = sId;
    }

    public String getiId() {
        return iId;
    }

    public void setiId(String iId) {
        this.iId = iId;
    }

    public List<StixDataObject> getObjectArray() {
        return objectArray;
    }

    public void setObjectArray(List<StixDataObject> objectArray) {
        this.objectArray = objectArray;
    }
}
