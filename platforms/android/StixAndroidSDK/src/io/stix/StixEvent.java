package io.stix;

import android.util.Log;

import java.util.Map;

import io.stix.rest.RestClient;
import io.stix.rest.model.AppEvent;
import io.stix.rest.model.AppInstall;
import io.stix.util.GsonUtil;

/**
 * NOTE: The current implementation of StixEvent requires the App User to be authenticated
 * Stix API will now automatically create an ApiAppUser on App install
 */
public class StixEvent {


    public static void send(String key, Map<String, String> dimensions) {
        send(key, null, dimensions);
    }

    public static void send(String key, String value) {
        send(key, value, null);
    }

    public static void send(String key) {
        send(key, null, null);
    }

    public static void send(String key, String value, Map<String, String> dimensions) {

        if (key == null)
            throw new RuntimeException("Invalid call to StixEvent. Key must not be null");

        AppInstall install = Stix.getInstall();
        if (install != null) {
            AppEvent event = new AppEvent();
            event.setiId(install.getiId());
            event.setuId(install.getuId());
            event.setKey(key);
            event.setDimensions(dimensions);
            event.setValue(value);
            RestClient.getInstance().postUserEvent(event, null);
            Log.d(Stix.TAG, "Sent StixEvent with key: " + key);
        } else
            Log.e(Stix.TAG, "Error. Attempted to save event without initialising Stix");
    }

    public static void send(String key, Object eventBean) {

        if (key == null)
            throw new RuntimeException("Invalid call to StixEvent. Key must not be null");

        if (eventBean == null)
            throw new RuntimeException("Invalid call to StixEvent. The Event Bean must not be null");

        AppInstall install = Stix.getInstall();
        if (install != null) {
            AppEvent event = new AppEvent();
            event.setiId(install.getiId());
            event.setuId(install.getuId());
            event.setKey(key);
            event.setValue(GsonUtil.gson.toJson(eventBean));

            RestClient.getInstance().postUserEvent(event, null);


            Log.d(Stix.TAG, "Sent StixEvent with key: " + key);
        } else
            Log.e(Stix.TAG, "Error. Attempted to save event without initialising Stix");
    }

}
