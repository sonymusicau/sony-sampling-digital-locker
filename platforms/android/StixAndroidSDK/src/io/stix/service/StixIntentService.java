package io.stix.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import io.stix.R;
import io.stix.Stix;
import io.stix.receiver.StixBroadcastReceiver;
import io.stix.rest.StixMessage;


public class StixIntentService extends IntentService {

	public static final int NOTIFICATION_ID = 1;
	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;

	public StixIntentService() {
		super("StixIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();

		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		String messageType = gcm.getMessageType(intent);

        Log.d(Stix.TAG, "Received: " + extras.toString());

		if (!extras.isEmpty()) { // has effect of unparcelling Bundle
			/*
			 * Filter messages based on message type. Since it is likely that
			 * GCM will be extended in the future with new message types, just
			 * ignore any message types you're not interested in, or that you
			 * don't recognize.
			 */
//			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
//				sendNotification("Send error: " + extras.toString());
//			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
//				sendNotification("Deleted messages on server: " + extras.toString());
//				// If it's a regular GCM message, do some work.
//			} else
            if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
				
				sendNotification(extras.getString("default"));

			}
		}

		// Release the wake lock provided by the WakefulBroadcastReceiver.
		StixBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void sendNotification(String jsonData) {
        try{
            Intent notificationIntent = this.getPackageManager().getLaunchIntentForPackage(this.getPackageName());
            notificationIntent.putExtra(Stix.MESSAGE_KEY, jsonData);
            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);


            JSONObject msg = new JSONObject(jsonData);
            String msgBody = msg.getString(StixMessage.BODY);
            String messageTitle = getOptionalString(msg, StixMessage.TITLE);
            String iconUrl = getOptionalString(msg, StixMessage.ICON);

            if(messageTitle == null)
                messageTitle = getAppNameForTitle();

            mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this).setContentTitle(messageTitle)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(msgBody)).setContentText(msgBody);

            if(iconUrl != null)
                mBuilder.setLargeIcon(getBitmapFromURL(iconUrl));


            mBuilder.setContentIntent(contentIntent);
            mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        }
        catch (Exception e){
            Log.e(Stix.TAG, "Notification error: " + e.getMessage(), e);
        }
	}

    private String getOptionalString(JSONObject o, String key){
        try {
            return o.has(key) ? o.getString(key) : null;
        }
        catch (Exception e){
            Log.e(Stix.TAG, e.getMessage(), e);
            return null;
        }
    }

    private String getAppNameForTitle(){
        PackageManager packageManager = getApplicationContext().getPackageManager();
        ApplicationInfo applicationInfo = null;
        try {
            applicationInfo = packageManager.getApplicationInfo(this.getPackageName(), 0);
        } catch (final PackageManager.NameNotFoundException e) {}

        return (String)((applicationInfo != null) ? packageManager.getApplicationLabel(applicationInfo) : "New Notification");
    }
    private Bitmap getBitmapFromURL(String strURL) {
        Bitmap myBitmap = null;

        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (Exception e) {
            Log.e(Stix.TAG, "Get Bitmap " + e.getMessage(),e);
            return myBitmap;
        }
    }

    public Bitmap convertToBitmap(Drawable drawable, int widthPixels, int heightPixels) {
        Bitmap mutableBitmap = Bitmap.createBitmap(widthPixels, heightPixels, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mutableBitmap);
        drawable.setBounds(0, 0, widthPixels, heightPixels);
        drawable.draw(canvas);

        return mutableBitmap;
    }
}
