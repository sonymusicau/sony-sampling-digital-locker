package io.stix;

import android.util.Log;

import java.util.List;

import io.stix.rest.RestClient;
import io.stix.rest.model.AppInstall;

/**
 * Created by James on 1/09/2014.
 */
public class StixData {

    public static void saveBatch(List<StixDataObject> stixDataObjectList) {

        AppInstall install = Stix.getInstall();
        if (install != null) {

            StixDataObjectBatch batch = new StixDataObjectBatch(install.getsId(), install.getiId(), stixDataObjectList);
            RestClient.getInstance().postData(batch, null);
        } else
            Log.e(Stix.TAG, "Error. Attempted to save data without initialising Stix");
    }
}
