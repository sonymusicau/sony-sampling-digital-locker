/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

//
//  MainViewController.h
//  Sony Sampling Digital Locker
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import "MainViewController.h"
#import <StixSDK/StixSDK-Swift.h>
#import "AppDelegate.h"

// TODO change from AOD to Spotlight keys
NSString* const STIX_API_KEY = @"eaa9b16dcc32056d0414bf28e24a55e3";
NSString* const STIX_API_SECRET = @"9dcda1b49922ff20ee77910b402b5819";

@implementation MainViewController

- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Uncomment to override the CDVCommandDelegateImpl used
        // _commandDelegate = [[MainCommandDelegate alloc] initWithViewController:self];
        // Uncomment to override the CDVCommandQueue used
        // _commandQueue = [[MainCommandQueue alloc] initWithViewController:self];
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        // Uncomment to override the CDVCommandDelegateImpl used
        // _commandDelegate = [[MainCommandDelegate alloc] initWithViewController:self];
        // Uncomment to override the CDVCommandQueue used
        // _commandQueue = [[MainCommandQueue alloc] initWithViewController:self];
    }

    stix = [[Stix alloc] initWithApiKey:STIX_API_KEY apiSecret:STIX_API_SECRET];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveCDVRemoteNotification:)
                                                 name:@"CDVRemoteNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveCDVRemoteNotificationError:)
                                                 name:@"CDVRemoteNotificationError"
                                               object:nil];
    
    return self;
}

- (void) receiveCDVRemoteNotification:(NSNotification *) notification
{
    // delay send so location values are set
    // (This works for app open only. The stix endpoints need to be revised as they're more approp for Android.
    // With ios we don't have all the data immediately and it depends on the user allowing access via alerts,
    // whereas with Android they accept all the permissions before installation.
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        NSString * latitude = @"";
        NSString * longitude = @"";
        
        if([CLLocationManager locationServicesEnabled]){
            
            AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
            if(appDelegate.currentLocation != nil) {
                latitude = [NSString stringWithFormat:@"%g", appDelegate.currentLocation.coordinate.latitude];
                longitude = [NSString stringWithFormat:@"%g", appDelegate.currentLocation.coordinate.longitude];
            }
        }
        
        // Received push token, sent it to stix to generate endpointArn
        [stix registerInstallOrOpen:[notification object] latitude: latitude longitude: longitude completion:^(AppInstall *appInstall){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // install success
                // pass endpointArn to phonegap javascript
                [webViewRef stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"setEndpointArn('%@');", appInstall.endpointArn]];
            });

            if([appInstall newInstall]) {
                DataCapture *dc = [[DataCapture alloc] init];
                [dc captureAndSend:stix appInstall: appInstall];
            }
        }];
    });
}

- (void) receiveCDVRemoteNotificationError:(NSNotification *) notification
{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        NSString * latitude = @"";
        NSString * longitude = @"";
    
        if([CLLocationManager locationServicesEnabled]){
            AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
            if(appDelegate.currentLocation != nil) {
                latitude = [NSString stringWithFormat:@"%g", appDelegate.currentLocation.coordinate.latitude];
                longitude = [NSString stringWithFormat:@"%g", appDelegate.currentLocation.coordinate.longitude];
            }
        }

        [stix registerInstallOrOpen:@"" latitude: latitude longitude: longitude completion:^(AppInstall *appInstall){
            // install success
            if([appInstall newInstall]) {
                DataCapture *dc = [[DataCapture alloc] init];
                [dc captureAndSend:stix appInstall: appInstall];
            }
        }];
    });
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}

#pragma mark View lifecycle

- (void)viewWillAppear:(BOOL)animated
{
    // View defaults to full size.  If you want to customize the view's size, or its subviews (e.g. webView),
    // you can do so here.

    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return [super shouldAutorotateToInterfaceOrientation:interfaceOrientation];
}

/* Comment out the block below to over-ride */

/*
- (UIWebView*) newCordovaViewWithFrame:(CGRect)bounds
{
    return[super newCordovaViewWithFrame:bounds];
}
*/

#pragma mark UIWebDelegate implementation

- (void)webViewDidFinishLoad:(UIWebView*)theWebView
{
    webViewRef = theWebView;
    
    // Black base color for background matches the native apps
    theWebView.backgroundColor = [UIColor blackColor];
   
    return [super webViewDidFinishLoad:theWebView];
}

/* Comment out the block below to over-ride */

/*

- (void) webViewDidStartLoad:(UIWebView*)theWebView
{
    return [super webViewDidStartLoad:theWebView];
}

- (void) webView:(UIWebView*)theWebView didFailLoadWithError:(NSError*)error
{
    return [super webView:theWebView didFailLoadWithError:error];
}

- (BOOL) webView:(UIWebView*)theWebView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
{
    return [super webView:theWebView shouldStartLoadWithRequest:request navigationType:navigationType];
}
*/
@end

@implementation MainCommandDelegate

/* To override the methods, uncomment the line in the init function(s)
   in MainViewController.m
 */

#pragma mark CDVCommandDelegate implementation

- (id)getCommandInstance:(NSString*)className
{
    return [super getCommandInstance:className];
}

- (NSString*)pathForResource:(NSString*)resourcepath
{
    return [super pathForResource:resourcepath];
}

@end

@implementation MainCommandQueue

/* To override, uncomment the line in the init function(s)
   in MainViewController.m
 */
- (BOOL)execute:(CDVInvokedUrlCommand*)command
{
    return [super execute:command];
}

@end
