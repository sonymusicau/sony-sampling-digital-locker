//
//  AlertList.m
//  SanityMusicLocker
//
//  Created by RU Admin on 29/05/2015.
//
//

#import "AlertList.h"
#import <Cordova/CDV.h>

@implementation AlertList


- (void)alertlist:(CDVInvokedUrlCommand*)command
{
    NSString* callbackId = command.callbackId;
    NSString* message = [command argumentAtIndex:1];
    message = [message stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
    message = [@"\n" stringByAppendingString:message];
    NSString* title = [command argumentAtIndex:0];
    
    CDVAlertList* alertList = [[CDVAlertList alloc]
                               initWithTitle:title
                               message:message
                               delegate:self
                               cancelButtonTitle:nil
                               otherButtonTitles:[command argumentAtIndex:2],[command argumentAtIndex:3],[command argumentAtIndex:4],nil];
    
    alertList.callbackId = callbackId;
    
    [alertList show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    CDVAlertList* cdvAlertList = (CDVAlertList*)alertView;
    CDVPluginResult* result;
    
    result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsInt:(int)(buttonIndex)];
    
    [self.commandDelegate sendPluginResult:result callbackId:cdvAlertList.callbackId];
}

@end

@implementation CDVAlertList

@synthesize callbackId;

@end
