//
//  AlertList.h
//  SanityMusicLocker
//
//  Created by RU Admin on 29/05/2015.
//
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioServices.h>
#import <Cordova/CDVPlugin.h>

@interface AlertList : CDVPlugin <UIAlertViewDelegate>{}

- (void)alertlist:(CDVInvokedUrlCommand*)command;

@end

@interface CDVAlertList : UIAlertView {}
@property (nonatomic, copy) NSString* callbackId;

@end
