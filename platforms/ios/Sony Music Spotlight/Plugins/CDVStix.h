#import <Cordova/CDVPlugin.h>
#import <StixSDK/StixSDK-Swift.h>

@interface CDVStix : CDVPlugin

- (void)registerInstallOrOpen:(CDVInvokedUrlCommand*)command;

@end
