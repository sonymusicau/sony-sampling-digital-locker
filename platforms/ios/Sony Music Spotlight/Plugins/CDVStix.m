#import "CDVStix.h"
#import <Cordova/CDV.h>
#import <CoreLocation/CoreLocation.h>
#import "AppDelegate.h"
#import "MainViewController.h"
#import <StixSDK/StixSDK-Swift.h>

@implementation CDVStix

/*
The user grants permission for Push Notifications via clicking the "Allow" button on an alert dialog when the app loads.
There is a callback in the AppDelegate which receives this and calls Stix install/open with the push token.
If the user clicks the "Don't Allow" button, there is no callback available ie there is no way to know this button was clicked.
Instead, the app's javascript will call this Phonegap Plugin. This plugin reads a system property which indicates if push notifications
were enabled. If not enabled, we will call Stix ourselves from this plugin. If enabled, we'll do nothing, as the callback on the "Allow"
button will have already called Stix for us.
*/
- (void)registerInstallOrOpen:(CDVInvokedUrlCommand*)command
{
    if([[UIApplication sharedApplication] currentUserNotificationSettings] == nil ||
       [[UIApplication sharedApplication] currentUserNotificationSettings].types == UIUserNotificationTypeNone) {
        
        Stix *stix = [[Stix alloc] initWithApiKey:STIX_API_KEY apiSecret:STIX_API_SECRET];
        
        NSString * latitude = @"";
        NSString * longitude = @"";
        
        if([CLLocationManager locationServicesEnabled]){
            AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
            if(appDelegate.currentLocation != nil) {
                latitude = [NSString stringWithFormat:@"%g", appDelegate.currentLocation.coordinate.latitude];
                longitude = [NSString stringWithFormat:@"%g", appDelegate.currentLocation.coordinate.longitude];
            }
        }
        
        [stix registerInstallOrOpen:@"" latitude:latitude longitude:longitude completion:^(AppInstall *appInstall) {
            // install success
            if([appInstall newInstall]) {
                DataCapture *dc = [[DataCapture alloc] init];
                [dc captureAndSend:stix appInstall: appInstall];
            }
        }];
    }
}

@end
