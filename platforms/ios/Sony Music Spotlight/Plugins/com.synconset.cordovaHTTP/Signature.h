//
//  Signature.h
//
//  Created by vincent song on 6/09/11.
//  Copyright 2011 RedUnicorn.com. All rights reserved.
//

@interface Signature : NSObject

+(NSString*)getAppSignatureWithAppSecretKey:(NSString*)appSecretKey EndpointName:(NSString*)endpointName UTC:(unsigned long long)utc EndpointParam:(NSDictionary*)endpointParam;

+(NSString*)getUserSignatureWitAppScretKey:(NSString*)appSecretKey UserSecretKey:(NSString*)userSecretKey EndpointName:(NSString*)endpointName UTC:(unsigned long long)utc EndpointParam:(NSDictionary*)endpointParam;

+(NSString*) escapeSpecialCharactersForString:(NSString*)sourceStr;

@end
