//
//  Signature.m
//
//  Created by vincent song on 6/09/11.
//  Copyright 2011 RedUnicorn.com. All rights reserved.
//

#import "Signature.h"
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonHMAC.h>

#define kENDPOINT_PARAM_NAME   @"endpoint"
#define kUTC_PARAM_NAME            @"utc"

@interface Signature (private)
+(NSString*) getSignatureContentWithUTC:(unsigned long long)utc  EndpointName:(NSString*)endpointName EndpointParam:(NSDictionary*)endpointParam;
+(NSString*) hmacSHA256:(NSString*)message Key:(NSString*)key;
@end

@implementation Signature

+(NSString*) escapeSpecialCharactersForString:(NSString*)sourceStr {
    //CFStringRef value =CFURLCreateStringByAddingPercentEscapes(
    //                                                           NULL,
    //                                                           (CFStringRef)sourceStr,
     //                                                          NULL,
      //                                                         (CFStringRef)@"+",
       //                                                        kCFStringEncodingUTF8);
    //NSString *resultStr = [[(NSString*)value copy] autorelease];
    //CFRelease(value);
    //return resultStr;
    return sourceStr;
}

+(NSString*) getSignatureContentWithUTC:(unsigned long long)utc  EndpointName:(NSString*)endpointName EndpointParam:(NSDictionary*)endpointParam {

    NSString* sig = [NSString stringWithFormat:@"%@=%@&%@=%qu", kENDPOINT_PARAM_NAME, endpointName, kUTC_PARAM_NAME, utc];
    if (endpointParam.count > 0) {
        NSArray *sortedKeys = [[endpointParam allKeys] sortedArrayUsingSelector:@selector(compare:)];
        for (NSString *key in sortedKeys) {
            if([key isEqualToString:@"authUserKey"] || [key isEqualToString:@"authUserSecret"]) {
                continue;
            }
            sig = [sig stringByAppendingFormat:@"&%@=%@", key, [endpointParam valueForKey:key]];
        }
    }
    return sig;
}

//+(NSString*) hmacSHA256:(NSString*)message Key:(NSString*)key {
//    
//    CC_SHA256_CTX ctx;
//    CC_SHA256_Init(&ctx);
//    
//    unsigned char hashedKey[64] = {0};
//    
//    if (key.length > 64) {
//        CC_SHA256_Update(&ctx, (void *)[key UTF8String], [key length]);
//        CC_SHA256_Final(hashedKey, &ctx);
//        CC_SHA256_Init(&ctx);
//    }else{
//        strcpy((char*)hashedKey, [key UTF8String]);
//    }
//    
//    unsigned char block[64] = {0};
//    for (int i = 0; i < strlen((char*)hashedKey); ++i) {
//	    block[i] = hashedKey[i] ;
//    }
//    
//	for (int i = strlen((char*)hashedKey); i < 64; ++i) {
//	    block[i] = 0;
//    }
//    
//    for (int i = 0; i < 64; ++i)
//	    block[i] ^= 0x36;
//	
//    CC_SHA256_Update(&ctx, (void *)block, 64);
//    CC_SHA256_Update(&ctx, (void *)[message UTF8String], message.length);
//    
//    unsigned char hashBytes[64] = {0};
//    CC_SHA256_Final(hashBytes, &ctx);
//    CC_SHA256_Init(&ctx);
//    
//    for (int i = 0; i < 64; ++i) {
//	    block[i] ^= (0x36 ^ 0x5c);
//    }
//    
//	CC_SHA256_Update(&ctx, (void*)block, 64);
//	CC_SHA256_Update(&ctx, (void*)hashBytes, strlen((char*)hashBytes));
//	CC_SHA256_Final(hashBytes, &ctx);;
//    CC_SHA256_Init(&ctx);
//    
//	// The outer hash is the message signature...
//	// convert its bytes to hexadecimals.
//	char *hexadecimals = malloc(strlen((char*)hashBytes)*2+1);
//    memset(hexadecimals, 0, strlen((char*)hashBytes)*2+1);
//	for (int i = 0; i < strlen((char*)hashBytes); ++i)
//	{
//	    for (int j = 0; j < 2; ++j)
//	    {
//            int value = (hashBytes[i] >> (4 - 4 * j)) & 0xf;
//            char base = (value < 10) ? ('0') : ('a' - 10);
//            hexadecimals[i * 2 + j] = (char) (base + value);
//	    }
//	}
//    
//	// Return a hexadecimal string representation of the message signature.
//    NSString *resStr = [NSString stringWithUTF8String:hexadecimals];
//    free(hexadecimals);
//	return resStr;
//}

+(NSString*)convert2Hex:(char*)bytes Length:(int)length {
    char *hexadecimals = malloc(length*2+1);
    memset(hexadecimals, 0, length*2+1);
	for (int i = 0; i < length; ++i)
	{
	    for (int j = 0; j < 2; ++j)
	    {
            int value = (bytes[i] >> (4 - 4 * j)) & 0xf;
            char base = (value < 10) ? ('0') : ('a' - 10);
            hexadecimals[i * 2 + j] = (char) (base + value);
	    }
	}
    
	// Return a hexadecimal string representation of the message signature.
    NSString *resStr = [NSString stringWithUTF8String:hexadecimals];
    free(hexadecimals);
	return resStr;

}


+(NSString*)getAppSignatureWithAppSecretKey:(NSString*)appSecretKey EndpointName:(NSString*)endpointName UTC:(unsigned long long)utc EndpointParam:(NSDictionary*)endpointParam {
    NSString *sig = [Signature getSignatureContentWithUTC:utc EndpointName:endpointName EndpointParam:endpointParam];
    char hmacDigest[CC_SHA256_DIGEST_LENGTH] = {0};
    const char* sigStr = [sig UTF8String];
    CCHmac(kCCHmacAlgSHA256, [appSecretKey UTF8String], appSecretKey.length, sigStr, strlen(sigStr), hmacDigest);
    return [Signature convert2Hex:hmacDigest Length:CC_SHA256_DIGEST_LENGTH];

}

+(NSString*)getUserSignatureWitAppScretKey:(NSString*)appSecretKey UserSecretKey:(NSString*)userSecretKey EndpointName:(NSString*)endpointName UTC:(unsigned long long)utc EndpointParam:(NSDictionary*)endpointParam {
    NSString *sig = [Signature getSignatureContentWithUTC:utc EndpointName:endpointName EndpointParam:endpointParam];
    char hmacDigest[CC_SHA256_DIGEST_LENGTH] = {0};
    NSString *key = [NSString stringWithFormat:@"%@&%@", appSecretKey, userSecretKey];
    const char* sigStr = [sig UTF8String];
    CCHmac(kCCHmacAlgSHA256, [key UTF8String], key.length, sigStr, strlen(sigStr), hmacDigest);
    return [Signature convert2Hex:hmacDigest Length:CC_SHA256_DIGEST_LENGTH];
    
}


@end
